"""Module for calculating FNV hash values using the FNV-1 algorithm."""
from typing import Union

# FNV sizes that are xor folds of other sizes.
_fnv_xor_folds = {
    24: 32,
}

# FNV parameters for supported sizes.
_fnv_parameters = {
    32: (2166136261, 16777619),
    64: (14695981039346656037, 1099511628211),
}


def fnv(size: int, data: Union[str, bytes]) -> int:
    """Calculates the FNV-1 hash of a string of bytestring.

    If a string is given, it is first lowercased and encoded as UTF-8.

    Args:
        size: The hash size to compute.
        data: The string to hash.

    Returns:
        The computed hash

    Raises:
        ValueError: If the hash size is unsupported.
    """
    # Convert strings to appropriate bytes.
    if isinstance(data, str):
        data = data.lower().encode()
    # Identify the hashing size.
    hash_size = _fnv_xor_folds.get(size, size)
    # Get the FNV parameters.
    if hash_size not in _fnv_parameters:
        raise ValueError(f"Unsupported FNV size: {size}")
    offset_basis, prime = _fnv_parameters[hash_size]
    # Mask to remove overflow.
    hash_mask = (1 << hash_size) - 1
    # Main hashing algorithm.
    hash = offset_basis
    for byte in data:
        hash = hash * prime
        hash = hash ^ byte
        hash = hash & hash_mask
    # Xor-fold the result if required.
    if size < hash_size:
        fold_mask = (1 << size) - 1
        hash = (hash >> size) ^ (hash & fold_mask)
    # Return the final result.
    return hash
