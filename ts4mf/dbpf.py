"""Module for working with database package files."""
from dataclasses import dataclass
from functools import partial
from itertools import chain
import os
from struct import calcsize, unpack
from typing import (
    Callable,
    Dict,
    IO,
    Iterator,
    List,
    MutableMapping,
    SupportsBytes,
    Tuple,
    Union,
)
import zlib

from ts4mf.resource import ResourceKey, to_resource_key
from ts4mf.util.asserts import assert_uint_size
from ts4mf.util.io import fpack, funpack, funpack1
from ts4mf.util.openenum import OpenEnum


def decompress_internal_compression(source: bytes) -> bytes:
    """Decompresses data compressed using internal compression.

    Args:
        source: The compressed data.

    Returns:
        The decompressed data.
    """
    # Read the header.
    # size: The size of the uncompressed data.
    # sp: The current source position.
    if source[0] & 0x80:
        (size,) = unpack(">I", source[2:6])
        sp = 6
    else:
        (size,) = unpack(">I", b"\0" + source[2:5])
        sp = 5
    # Define additional variables.
    # dest: Array for storing the decompressed data.
    # dp: The current dest position.
    # last_command: Whether the last decompression command is issued.
    dest = bytearray(size)
    dp = 0
    last_command = False
    # Decompress the data.
    while not last_command:
        # Read the next decompression command.
        # sn: Number of bytes to copy from source.
        # dn: Number of bytes to copy from dest.
        # do: Offset for copying from dest.
        b0, sp = source[sp], sp + 1
        if b0 < 0x80:
            b1, sp = source[sp], sp + 1
            sn = b0 & 0x03
            dn = ((b0 & 0x1C) >> 2) + 3
            do = ((b0 & 0x60) << 3) + b1 + 1
        elif b0 < 0xC0:
            b1, b2, sp = source[sp], source[sp + 1], sp + 2
            sn = (b1 & 0xC0) >> 6
            dn = (b0 & 0x3F) + 4
            do = ((b1 & 0x3F) << 8) + b2 + 1
        elif b0 < 0xE0:
            b1, b2, b3, sp = source[sp], source[sp + 1], source[sp + 2], sp + 3
            sn = b0 & 0x03
            dn = ((b0 & 0x0C) << 6) + b3 + 5
            do = ((b0 & 0x10) << 12) + (b1 << 8) + b2 + 1
        elif b0 < 0xFC:
            sn = ((b0 & 0x1F) << 2) + 4
            dn, do = 0, 0
        else:
            sn, dn, do = b0 & 0x03, 0, 0
            last_command = True
        # Copy from source to dest.
        dest[dp : dp + sn] = source[sp : sp + sn]
        dp, sp = dp + sn, sp + sn
        # Copy from dest to dest (must be done byte by byte).
        for _ in range(dn):
            dest[dp] = dest[dp - do]
            dp += 1
    # Return the final result.
    return bytes(dest)


class Compression(OpenEnum):
    """Compression types.

    Args:
        value: A 16 bit compression type id.
    """

    NONE = 0x0000
    ZLIB = 0x5A42
    DELETED = 0xFFE0
    STREAMABLE = 0xFFFE
    INTERNAL = 0xFFFF

    # Limit to 16 bit unsigned integers.
    _generate_value_ = partial(assert_uint_size, 16, source="Compression")


class UnsupportedCompression(Exception):
    """Raised when an unsupported compression type is used."""


@dataclass(frozen=True)
class _Resource:
    """Resource extracted from or to be inserted into a package."""

    _data: bytes
    _compression: Compression
    _uncompressed_size: int

    def __bytes__(self) -> bytes:
        if self._compression == Compression.NONE:
            return self._data
        if self._compression == Compression.ZLIB:
            return zlib.decompress(self._data)
        if self._compression == Compression.INTERNAL:
            return decompress_internal_compression(self._data)
        raise UnsupportedCompression(f"Unsuppored compression: {self._compression}")


@dataclass(frozen=True)
class _IndexEntry:
    """Package index entry, describes a resource in the package."""

    position: int
    compressed_size: int
    uncompressed_size: int
    compression: Compression
    committed: int


class InvalidPackage(Exception):
    """Raised when an invalid or unsupported package is opened."""


class InvalidOperation(Exception):
    """Raised when an invalid operation is used on a package."""


class Package(MutableMapping[Union[str, tuple, ResourceKey], SupportsBytes]):
    """Represents a database package file."""

    def __init__(self, file: Union[str, IO[bytes]], mode: str = "r") -> None:
        """
        Args:
            file: Either a file path or a binary file object.
            mode: One of the following modes to open the file in:
                'r': Open a package for reading.
                'w': Create a new package for writing.
                'a': Open a package for reading and writing,
                    or create a new package if it doesn't exist.
                'x': Create a new package for writing,
                    but raise an exception if the file already exists
                    (acts as 'w' for binary file objects).
        """
        # Handle mode argument.
        if mode not in ["r", "w", "a", "x"]:
            raise ValueError(f"Invalid file mode: {mode}")
        self._mode = mode
        # Handle file argument.
        if isinstance(file, str):
            if self._mode == "a" and not os.path.exists(file):
                self._mode = "w"
            file_mode = "r+b" if self._mode == "a" else self._mode + "b"
            file = open(file, file_mode)
        if not file.seekable:
            raise ValueError("The file must be seekable")
        if self._mode in ["r", "a"] and not file.readable:
            raise ValueError("The file must be readable")
        if self._mode in ["w", "a", "x"] and not file.writable:
            raise ValueError("The file must be writeable")
        self._file = file
        # Setup index and resource staging.
        self._index: Dict[ResourceKey, _IndexEntry] = {}
        self._insertions: Dict[ResourceKey, _Resource] = {}
        # Read header and index if existing package.
        if self._mode in ["r", "a"]:
            self._read()
        # Set status flag.
        self._committed = self._mode in ["r", "a"]

    @property
    def closed(self) -> bool:
        """Whether the package has been closed."""
        return self._file.closed

    @property
    def committed(self) -> bool:
        """Whether the package content is committed."""
        return self._committed

    def resources(
        self, key_pred: Callable[[ResourceKey], bool] = None
    ) -> Iterator[Tuple[ResourceKey, SupportsBytes]]:
        """Creates a generator for getting package resources.

        Args:
            key_pred: A predicate to filter resource keys.

        Returns:
            A generator for getting the resources.
        """
        return ((k, self[k]) for k in self if key_pred(k))  # type: ignore

    def commit(self) -> None:
        """Commits the changes to disk."""
        if self.committed:
            return
        if self.closed:
            raise InvalidOperation("Operation on a closed package")
        if self._mode in ["w", "a", "x"]:
            self._write()
            self._committed = True

    def close(self) -> None:
        """Closes the package without committing the changes."""
        self._file.close()

    def _read(self) -> None:
        """Reads the package metadata."""
        # Seek to the header.
        self._file.seek(0)
        # Check the file identifier.
        if self._file.read(4) != b"DBPF":
            # If we are in append mode, treat the file as empty
            # and switch to write mode.
            if self._mode == "a":
                self._mode = "w"
                return
            raise InvalidPackage("Not a package file")
        # Check the package version.
        package_version = funpack("<II", self._file)
        if package_version != (2, 1):
            major, minor = package_version
            raise InvalidPackage(f"Unsopported version: {major}.{minor}")
        # Read the rest of the header, ignoring most fields.
        (index_count, pos_low, pos_high) = funpack("< 24x I I 20x Q 24x", self._file)
        index_position = pos_low or pos_high
        # Seek to the index.
        self._file.seek(index_position)
        # Read the index header.
        index_flags = funpack1("<I", self._file)
        const_type = funpack1("<I", self._file) if index_flags & 0x1 else None
        const_group = funpack1("<I", self._file) if index_flags & 0x2 else None
        const_ext = funpack1("<I", self._file) if index_flags & 0x4 else None
        # Read the index entries.
        for _ in range(index_count):
            # Read resource key.
            type = funpack1("<I", self._file) if const_type is None else const_type
            group = funpack1("<I", self._file) if const_group is None else const_group
            ext = funpack1("<I", self._file) if const_ext is None else const_ext
            base = funpack1("<I", self._file)
            instance = (ext << 32) + base
            key = ResourceKey(type, group, instance)
            # Read index entry.
            position, comp_size, uncomp_size = funpack("<III", self._file)
            if not comp_size & 0x80000000:
                raise InvalidPackage("Only extended compression is supported")
            comp_size = comp_size & 0x7FFFFFFF
            compression, committed = funpack("<HH", self._file)
            entry = _IndexEntry(
                position, comp_size, uncomp_size, Compression(compression), committed,
            )
            # Store the entry.
            self._index[key] = entry

    def _calculate_holes(self, start: int) -> Tuple[List[Tuple[int, int]], int]:
        """Calculates the holes in the package.

        Args:
            start: The start of the resource list.

        Returns:
            A sorted list of size and position pairs for the holes
            and the position after the last resource.
        """
        # Get a list of resource positions and sizes sorted by position.
        resources = sorted(
            (e.position, e.compressed_size) for e in self._index.values()
        )
        # Calculate the holes (size, position).
        holes = []
        last_position = start
        for position, size in resources:
            if position > last_position:
                holes.append((position - last_position, last_position))
            last_position = position + size
        # Return the list of holes sorted.
        holes.sort()
        return holes, last_position

    def _write(self) -> None:
        """Writes inserted resources and updates the metadata."""
        # Skip the header until later.
        header_fmt = "< 4s II 24x I 4x I 12x I Q 24x"
        self._file.seek(calcsize(header_fmt))
        # Calculate a list of holes in the package.
        holes, last_position = self._calculate_holes(self._file.tell())
        # Write all resources and create corresponding index entries.
        for k, e in self._insertions.items():
            # Compress the data if uncompressed.
            if e._compression == Compression.NONE:
                data, comp = zlib.compress(e._data), Compression.ZLIB
            else:
                data, comp = e._data, e._compression
            # Get the size of the data.
            size = len(data)
            # Find a spot for the resource.
            for i, (s, p) in enumerate(holes):
                if size <= s:
                    position = p
                    # Update the hole list.
                    del holes[i]
                    if size < s:
                        holes.append((s - size, p + size))
                    break
            else:
                position = last_position
                last_position += size
            # Write the data to file.
            self._file.seek(position)
            self._file.write(data)
            # Add the resource to the index.
            self._index[k] = _IndexEntry(
                position, size, e._uncompressed_size, comp, True,  # type: ignore
            )
        # Clear the insertion list.
        self._insertions.clear()
        # Determine eventual constant key parts.
        const_type, const_group, const_ext = None, None, None
        index_flags, first_entry = 0, True
        for k in self._index:
            # Assume all parts constant at first entry.
            if first_entry:
                const_type = k.type.value
                const_group = k.group.value
                const_ext = k.instance.value >> 32
                index_flags, first_entry = 0x7, False
                continue
            # Unset any constant parts if they don't match.
            if const_type is not None and k.type.value != const_type:
                const_type, index_flags = None, index_flags - 0x1
            if const_group is not None and k.group.value != const_group:
                const_group, index_flags = None, index_flags - 0x2
            if const_ext is not None and (k.instance.value >> 32) != const_ext:
                const_ext, index_flags = None, index_flags - 0x4
            # Stop if no constant parts remain.
            if const_type is None and const_group is None and const_ext is None:
                break
        # Write the index header.
        self._file.seek(last_position)
        fpack("<I", self._file, index_flags)
        if const_type is not None:
            fpack("<I", self._file, const_type)
        if const_group is not None:
            fpack("<I", self._file, const_group)
        if const_ext is not None:
            fpack("<I", self._file, const_ext)
        # Write each index entry.
        for k, e in self._index.items():
            if const_type is None:
                fpack("<I", self._file, k.type.value)
            if const_group is None:
                fpack("<I", self._file, k.group.value)
            if const_ext is None:
                fpack("<I", self._file, k.instance.value >> 32)
            fpack(
                "<IIIIHH",
                self._file,
                k.instance.value & 0xFFFFFFFF,
                e.position,
                0x80000000 | e.compressed_size,
                e.uncompressed_size,
                e.compression.value,
                True,
            )
        # Calculate the index size.
        index_size = self._file.tell() - last_position
        # Truncate the rest of the file.
        self._file.truncate()
        # Write the header.
        self._file.seek(0)
        fpack(
            header_fmt,
            self._file,
            b"DBPF",
            *(2, 1),
            len(self._index),
            index_size,
            3,
            last_position,
        )

    def _extract(self, entry: _IndexEntry) -> SupportsBytes:
        """Extracts a resource from the package.

        Args:
            entry: The index entry for the resource.

        Returns:
            The resource.
        """
        # Seek to the resource.
        self._file.seek(entry.position)
        # Extract the data.
        data = self._file.read(entry.compressed_size)
        # Return the resource compressed.
        return _Resource(data, entry.compression, entry.uncompressed_size)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.commit()
        self.close()

    def __getitem__(self, key) -> SupportsBytes:
        if self.closed:
            raise InvalidOperation("Operation on a closed package")
        key = to_resource_key(key)
        if key in self._index:
            return self._extract(self._index[key])
        if key in self._insertions:
            return self._insertions[key]
        raise KeyError(f"Package does not contain {key}")

    def __setitem__(self, key, value) -> None:
        if self._mode == "r":
            raise InvalidOperation("Appending to a read-only package")
        if self.closed:
            raise InvalidOperation("Operation on a closed package")
        # Handle the key argument.
        key = to_resource_key(key)
        # Check value argument.
        if not isinstance(value, bytes) and not hasattr(value, "__bytes__"):
            raise ValueError("Package resource must be convertable to bytes")
        # Handle 'CompressedResource'.
        if isinstance(value, _Resource):
            self._insertions[key] = value
        # Handle everything else.
        else:
            data = bytes(value)
            self._insertions[key] = _Resource(
                data,
                Compression.NONE,  # type: ignore
                len(data),
            )
        # Remove the key from the index if present.
        if key in self._index:
            del self._index[key]
        # Mark package uncommitted.
        self._committed = False

    def __delitem__(self, key) -> None:
        if self._mode == "r":
            raise InvalidOperation("Deleting from a read-only package")
        if self.closed:
            raise InvalidOperation("Operation on a closed package")
        # Handle the key argument.
        key = to_resource_key(key)
        # Find and remove the resource, if it exists.
        if key in self._index:
            del self._index[key]
        elif key in self._insertions:
            del self._insertions[key]
        else:
            raise KeyError(f"Package does not contain {key}")
        # Mark package uncommitted.
        self._committed = False

    def __contains__(self, key) -> bool:
        key = to_resource_key(key)
        return key in self._index or key in self._insertions

    def __iter__(self) -> Iterator[ResourceKey]:
        return chain(self._index, self._insertions)

    def __len__(self) -> int:
        return len(self._index) + len(self._insertions)
