"""Command line tool for working with package files."""
from argparse import ArgumentParser
import os
from typing import List, Optional, Union

from ts4mf.console import console_command
from ts4mf.dbpf import Package
from ts4mf.resource import from_filename, to_filename

parser = ArgumentParser(description="Handle package files.")
subparsers = parser.add_subparsers(dest="command", required=True)
list_parser = subparsers.add_parser("list", help="List package resources.")
export_parser = subparsers.add_parser("export", help="Export resources from a package.")
import_parser = subparsers.add_parser(
    "import", help="Import resources into an existing package or new package."
)
delete_parser = subparsers.add_parser("delete", help="Delete resources from a package.")
copy_parser = subparsers.add_parser(
    "copy", help="Copy resources from a package to another package or a new package."
)

for subparser in [list_parser, export_parser, copy_parser]:
    subparser.add_argument("source", help="The source package path.")

for subparser in [list_parser, export_parser]:
    subparser.add_argument(
        "-n",
        "--naming",
        default="ts4mf",
        choices=["ts4mf", "maxis", "s4pe", "s4s"],
        help="The resource filename format.",
    )

for subparser in [import_parser, delete_parser, copy_parser]:
    subparser.add_argument("target", help="The target package path.")

for subparser in [import_parser, copy_parser]:
    group = subparser.add_mutually_exclusive_group()
    group.add_argument(
        "-w", "--overwrite", action="store_true", help="Overwrite the target package."
    )
    group.add_argument(
        "-x",
        "--no-overwrite",
        action="store_true",
        help="Ensure that the package does not already exist.",
    )

for subparser in [
    list_parser,
    export_parser,
    import_parser,
    delete_parser,
    copy_parser,
]:
    subparser.add_argument(
        "-t",
        "--type",
        action="append",
        help="Include resources by type (may be specified several times).",
    )
    subparser.add_argument(
        "-g",
        "--group",
        action="append",
        help="Include resources by group (may be specified several times).",
    )
    subparser.add_argument(
        "-i",
        "--instance",
        action="append",
        help="Include resources by instance (may be specified several times).",
    )

for subparser in [export_parser, import_parser, delete_parser, copy_parser]:
    subparser.add_argument(
        "resource",
        nargs="*",
        help="Names of resources to include in the action (in any supported formats).",
    )

for subparser in [export_parser, import_parser]:
    subparser.add_argument(
        "-d", "--dir", default=".", help="Directory for the resources."
    )


def convert_ids(arg: Optional[List[str]]) -> List[Union[int, str]]:
    res = []
    for v in arg or []:
        try:
            res.append(int(v, 0))
        except:
            res.append(v)
    return res


@console_command("mf.package", parser)
def main(args):
    types = convert_ids(args.type)
    groups = convert_ids(args.group)
    instances = convert_ids(args.instance)

    def key_pred(key):
        if types and key.type not in types:
            return False
        if groups and key.group not in groups:
            return False
        if instances and key.instance not in instances:
            return False
        return True

    if args.command == "list":
        with Package(args.source) as sp:
            for k in filter(key_pred, sp):
                print(to_filename(k, args.naming))
    elif args.command == "export":
        os.makedirs(args.dir, exist_ok=True)
        with Package(args.source) as sp:
            if args.resource:
                for name in args.resource:
                    with open(os.path.join(args.dir, name), "wb") as f:
                        f.write(bytes(sp[name]))
            else:
                for k, r in sp.resources(key_pred):
                    name = to_filename(k, args.naming)
                    with open(os.path.join(args.dir, name), "wb") as f:
                        f.write(bytes(r))
    elif args.command == "import":
        mode = "w" if args.overwrite else "x" if args.no_overwrite else "a"
        with Package(args.target, mode) as tp:
            if args.resource:
                for name in args.resource:
                    with open(os.path.join(args.dir, name), "rb") as f:
                        tp[name] = f.read()
            else:
                for name in os.listdir(args.dir):
                    try:
                        key = from_filename(name)
                    except:
                        continue
                    if not key_pred(key):
                        continue
                    with open(os.path.join(args.dir, name), "rb") as f:
                        tp[key] = f.read()
    elif args.command == "delete":
        with Package(args.target, "a") as tp:
            if args.resource:
                print("res")
                for name in args.resource:
                    print(name)
                    del tp[name]
            else:
                print("no-res")
                for k in tp:
                    print(k)
                    if key_pred(k):
                        print("match")
                        del tp[k]
    elif args.command == "copy":
        mode = "w" if args.overwrite else "x" if args.no_overwrite else "a"
        with Package(args.source) as sp, Package(args.target, mode) as tp:
            if args.resource:
                for name in args.resource:
                    key = from_filename(name)
                    tp[key] = sp[key]
            else:
                for k, r in sp.resources(key_pred):
                    tp[k] = r
