"""Command line tool for generating hashes."""
from argparse import ArgumentParser

from ts4mf.console import console_command
from ts4mf.hash import fnv

parser = ArgumentParser(description="Generate FNV-1 hashes.")
parser.add_argument(
    "size", type=int, choices=[24, 32, 64], help="The size of the hash."
)
parser.add_argument("string", help="The string to hash.")
parser.add_argument(
    "-f",
    "--format",
    default="hex",
    choices=["dec", "hex"],
    help="The output number format.",
)

formatters = {
    "dec": lambda size: str,
    "hex": lambda size: f"0x{{:0{size // 8}X}}".format,
}


@console_command("mf.hash", parser)
def main(args):
    res = fnv(args.size, args.string)
    print(formatters[args.format](args.size)(res))
