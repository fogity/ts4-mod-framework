"""Command line tool for working with string tables."""
from argparse import ArgumentParser
from configparser import ConfigParser
from contextlib import suppress
import json
import os
import re
from typing import Dict, Mapping, Union

import xml.dom.minidom

from ts4mf.console import console_command
from ts4mf.stbl import StringTable
from ts4mf.util.bidict import BiDict

format_to_ext = BiDict({"stbl": ".stbl", "ini": ".ini", "json": ".json", "xml": ".xml"})

parser = ArgumentParser(description="Handle string tables.")
parser.add_argument(
    "source", help="The source table path (may be in any supported formats)."
)
parser.add_argument("target", nargs="?", help="The target table path.")
parser.add_argument(
    "-f",
    "--format",
    choices=list(format_to_ext),
    help="File format of the produced table.",
)
parser.add_argument(
    "-w", "--overwrite", action="store_true", help="Overwrite any existing files."
)
parser.add_argument(
    "-s",
    "--search",
    action="append",
    default=[],
    help="Filter the strings using the phrase (may be specified multiple times).",
    metavar="PHRASE",
)
parser.add_argument(
    "-S",
    "--search-regex",
    action="append",
    default=[],
    help="Like '--search' but using regex.",
    metavar="PATTERN",
)
parser.add_argument(
    "-r",
    "--replace",
    action="append",
    nargs=2,
    default=[],
    help="Find and replace using the pharse and replacement "
    "(may be specified multiple times).",
    metavar=("PHRASE", "REPLACEMENT"),
)
parser.add_argument(
    "-R",
    "--replace-regex",
    action="append",
    nargs=2,
    default=[],
    help="Like '--replace' but using regex.",
    metavar=("PATTERN", "REPLACEMENT"),
)
parser.add_argument(
    "-a",
    "--all",
    action="store_true",
    help="Keep unmodified strings after find and replace.",
)


def identify_format(path: str) -> str:
    _, ext = os.path.splitext(path)
    format = format_to_ext.inv.get(ext.lower())
    return format or "stbl"


def adjust_path_by_format(path: str, source_format: str, target_format: str) -> str:
    base = path if source_format == "stbl" else os.path.splitext(path)[0]
    return base if target_format == "stbl" else base + format_to_ext[target_format]


def format_ids(old_dict: Mapping[Union[int, str], str]) -> Dict[str, str]:
    new_dict = {}
    for k, s in old_dict.items():
        if isinstance(k, int):
            k = f"0x{k:08X}"
        new_dict[k] = s
    return new_dict


def convert_ids(old_dict: Mapping[str, str]) -> Dict[Union[int, str], str]:
    new_dict = {}
    for k, s in old_dict.items():
        with suppress(ValueError):
            k = int(k, 0)
        new_dict[k] = s
    return new_dict


def to_dict(path: str, format: str) -> Dict[Union[int, str], str]:
    if format == "stbl":
        return {k.string or k.value: s for k, s in StringTable(path).items()}  # type: ignore
    if format == "ini":
        config = ConfigParser(interpolation=None)
        config.optionxform = str
        config.read(path)
        return convert_ids(config["Strings"])
    if format == "json":
        with open(path) as f:
            return convert_ids(json.load(f))
    if format == "xml":
        with xml.dom.minidom.parse(path) as doc:
            return convert_ids(
                {
                    element.getAttribute("key"): element.firstChild.nodeValue
                    for element in doc.getElementsByTagName("string")
                }
            )
    raise Exception("Internal error: can not read " + format)


def from_dict(path: str, format: str, strings: Dict[Union[int, str], str]) -> None:
    if format == "stbl":
        with open(path, "wb") as f:
            f.write(bytes(StringTable(strings)))
    elif format == "ini":
        with open(path, "w") as f:
            config = ConfigParser(interpolation=None)
            config.optionxform = str
            config["Strings"] = format_ids(strings)
            config.write(f)
    elif format == "json":
        with open(path, "w") as f:
            json.dump(format_ids(strings), f, indent="\t")
    elif format == "xml":
        doc = xml.dom.minidom.getDOMImplementation().createDocument(
            None, "string_table", None
        )
        root = doc.documentElement
        for k, s in format_ids(strings).items():
            element = doc.createElement("string")
            element.setAttribute("key", k)
            element.appendChild(doc.createTextNode(s))
            root.appendChild(element)
        with open(path, "wb") as f:
            f.write(doc.toprettyxml(encoding="utf-8"))
    else:
        raise Exception("Internal error: can not write " + format)


@console_command("mf.string_table", parser)
def main(args):
    # Identify if the results should be printed.
    print_results = not args.overwrite and not args.format and not args.target
    # Identify the source path and format.
    source_path, source_format = args.source, identify_format(args.source)
    # Identify the target path and format.
    target_format = args.format or (
        identify_format(args.target) if args.target else source_format
    )
    target_path = args.target or adjust_path_by_format(
        args.source, source_format, target_format
    )
    # Check if the target file may be written.
    if not print_results and not args.overwrite and os.path.exists(target_path):
        raise FileExistsError("The file " + target_path + " already exists")
    # Load the source table.
    table = to_dict(source_path, source_format)
    # Filter the table if searching.
    if args.search or args.search_regex:
        table = {
            k: s
            for k, s in table.items()
            if any((phrase in s for phrase in args.search))
            or any(re.search(pattern, s) for pattern in args.search_regex)
        }
    # Replace strings if replacing.
    if args.replace or args.replace_regex:
        new_table = {}
        for k, s in table.items():
            string = s
            for phrase, repl in args.replace:
                string = string.replace(phrase, repl)
            for pattern, repl in args.replace_regex:
                string = re.sub(pattern, repl, string)
            if args.all or string != s:
                new_table[k] = string
        table = new_table
    # Print the table if print option is selected otherwise write to file.
    if print_results:
        for k, s in format_ids(table).items():
            print(k, repr(s))
    else:
        from_dict(target_path, target_format, table)
