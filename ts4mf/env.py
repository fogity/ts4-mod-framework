"""Module for information about the game setup and execution environment."""
import os.path
import sys

from ts4mf.util.openenum import OpenEnum, name_as_value

# Flag for checking if the script is running within The Sims 4.
try:
    import sims4  # type: ignore

    running_within_ts4 = True
except ImportError:
    running_within_ts4 = False

# Flag for checking if the script is running on macOS.
running_on_macos = sys.platform == "darwin"


def ts4_game_dir() -> str:
    """Finds the game files directory.

    Returns:
        The path to the game directory.
    """
    # Compile a list of possible game directories.
    if running_on_macos:
        paths = [
            "/Application/The Sims 4.app/Contents",
            os.path.expanduser("~/Applications/The Sims 4.app/Contents"),
        ]
    else:
        raise Exception("Windows not yet supported")
    # Check if any of the directories exist.
    for path in paths:
        if os.path.isdir(path):
            return path
    raise Exception("Could not find the game directory")


class Pack(OpenEnum):
    """A content pack for the game."""

    # Defined in release order.
    FP01 = name_as_value()
    HOLIDAY_CELEBRATION_PACK = FP01
    GP01 = name_as_value()
    OUTDOOR_RETREAT = GP01
    EP01 = name_as_value()
    GET_TO_WORK = EP01
    SP01 = name_as_value()
    LUXURY_PARTY_STUFF = SP01
    SP02 = name_as_value()
    PERFECT_PATIO_STUFF = SP02
    GP02 = name_as_value()
    SPA_DAY = GP02
    SP03 = name_as_value()
    COOL_KITCHEN_STUFF = SP03
    SP04 = name_as_value()
    SPOOKY_STUFF = SP04
    EP02 = name_as_value()
    GET_TOGETHER = EP02
    SP05 = name_as_value()
    MOVIE_HANGOUT_STUFF = SP05
    SP06 = name_as_value()
    ROMANTIC_GARDEN_STUFF = SP06
    GP03 = name_as_value()
    DINE_OUT = GP03
    SP07 = name_as_value()
    KIDS_ROOM_STUFF = SP07
    SP08 = name_as_value()
    BACKYARD_STUFF = SP08
    EP03 = name_as_value()
    CITY_LIVING = EP03
    SP09 = name_as_value()
    VINTAGE_GLAMOUR_STUFF = SP09
    GP04 = name_as_value()
    VAMPIRES = GP04
    SP10 = name_as_value()
    BOWLING_STUFF = SP10
    GP05 = name_as_value()
    PARENTHOOD = GP05
    SP11 = name_as_value()
    FITNESS_STUFF = SP11
    SP12 = name_as_value()
    TODDLERS_STUFF = SP12
    EP04 = name_as_value()
    CATS_AND_DOGS = EP04
    SP13 = name_as_value()
    LAUNDRY_DAY_STUFF = SP13
    GP06 = name_as_value()
    JUNGLE_ADVENTURE = GP06
    SP14 = name_as_value()
    MY_FIRST_PET_STUFF = SP14
    EP05 = name_as_value()
    SEASONS = EP05
    EP06 = name_as_value()
    GET_FAMOUS = EP06
    GP07 = name_as_value()
    STRANGERVILLE = GP07
    EP07 = name_as_value()
    ISLAND_LIVING = EP07
    SP15 = name_as_value()
    MOSCHINO_STUFF = SP15
    GP08 = name_as_value()
    REALM_OF_MAGIC = GP08
    EP08 = name_as_value()
    DISCOVER_UNIVERSITY = EP08
