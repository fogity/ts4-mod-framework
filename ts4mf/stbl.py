"""Module for working with string tables."""
from functools import partial
from io import BytesIO
from struct import calcsize
from typing import Dict, IO, Iterator, Mapping, MutableMapping, Union

from ts4mf.hash import fnv
from ts4mf.util.asserts import assert_uint_size
from ts4mf.util.io import fpack, funpack, funpack1, to_stream
from ts4mf.util.openenum import OpenHashEnum


class StringTableKey(OpenHashEnum):
    """A key used to identify a localised string.

    Args:
        value: A key id or a key name to hash.
    """

    # Do not cache all key values.
    _cache_unnamed_ = False
    # Use 32 bit FNV-1 hash.
    _generate_hash_ = partial(fnv, 32)
    # Limit to 32 bit unsigned integers.
    _generate_value_ = partial(assert_uint_size, 32, source="StringTableKey")
    # Use hexadecimal format.
    _str_value_ = "0x{:08X}".format


class InvalidStringTable(Exception):
    """Raised when an invalid or unsupported string table is read."""


class StringTable(MutableMapping[Union[int, str, StringTableKey], str]):
    """Represents a string table."""

    def __init__(self, data=None) -> None:
        """
        If the argument is a byte source (file, filename or byte string),
        then it is parsed as a string table.
        If the argument is a dictionary, then it is used to create the table.

        Args:
            data: Either a byte source or a dictionary with strings.
        """
        self._dict: Dict[StringTableKey, str] = {}
        if isinstance(data, Mapping):
            for k, s in data.items():
                self[k] = s
        elif data is not None:
            with to_stream(data) as f:
                self._read(f)

    def _read(self, file: IO[bytes]) -> None:
        """Read a string table from a file.

        Args:
            file: File to read from.
        """
        # Check the file identifier.
        if file.read(4) != b"STBL":
            raise InvalidStringTable("Not a string table")
        # Check the string table version.
        version = funpack1("<H", file)
        if version != 5:
            raise InvalidStringTable(f"Unsupported version {version}")
        # Read the rest of the header.
        count = funpack1("< x Q 6x", file)
        # Read and add the strings.
        for _ in range(count):
            # Read string header.
            key, length = funpack("< I x H", file)
            # Read and add the string.
            self[key] = file.read(length).decode()

    def _write(self, file: IO[bytes]) -> None:
        """Writes a string table to a file.

        Args:
            file: File to write to.
        """
        # Skip the header until later.
        header_fmt = "< 4s H x Q 2x I"
        file.seek(calcsize(header_fmt))
        # Size of the strings in memory if null terminated.
        unpacked_size = 0
        # Write each string entry.
        for k, s in self._dict.items():
            string = s.encode()
            string_size = len(string)
            fpack("< I x H", file, k.value, string_size)
            file.write(string)
            unpacked_size += string_size + 1
        # Write the header.
        file.seek(0)
        fpack(header_fmt, file, b"STBL", 5, len(self), unpacked_size)

    def __bytes__(self) -> bytes:
        with BytesIO() as f:
            self._write(f)
            return f.getvalue()

    def __getitem__(self, key) -> str:
        return self._dict[StringTableKey.lookup(key)]

    def __setitem__(self, key, value: str) -> None:
        if not isinstance(value, str):
            raise ValueError("String table values must be strings")
        self._dict[StringTableKey.lookup(key)] = value

    def __delitem__(self, key) -> None:
        del self._dict[StringTableKey.lookup(key)]

    def __iter__(self) -> Iterator[StringTableKey]:
        return iter(self._dict)

    def __len__(self) -> int:
        return len(self._dict)
