"""Module with functions for asserting variable types and values."""


def assert_uint_size(
    size: int, value: int, *, source: str = None, note: str = None
) -> int:
    """Asserts that a value is an unsigned integer of a given size.

    Args:
        size: Integer size in number of bits.
        value: The value to check.
        source: The source of the assert.
        note: Extra details added at the end of the assert error.

    Returns:
        The value if valid.

    Raises:
        ValueError: If the value is invalid.
    """
    if isinstance(value, int) and 0 <= value <= (1 << size) - 1:
        return value
    prefix = source + " expected " if source else "Expected "
    middle = f"an unsigned {size} bit integer"
    suffix = " " + note if note else ""
    raise ValueError(prefix + middle + suffix)
