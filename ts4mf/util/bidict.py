"""Implementation of a bidirectional dictionary."""
from __future__ import annotations
from typing import Generic, Mapping, TypeVar

# Type parameters.
K, V = TypeVar("K"), TypeVar("V")


class BiDict(dict, Generic[K, V]):
    """A bidirectional dictionary.

    Attributes:
        inv: The inverse dictionary (also bidirectional).
    """

    def __init__(self, items: Mapping[K, V], _inv: BiDict[V, K] = None) -> None:
        """
        Args:
            items: A dictionary of items to populate the bidict,
                values must be unique.
        """
        super().__init__(items)
        if _inv is None:
            inv_items = {}
            for key, value in items.items():
                if value in inv_items:
                    raise ValueError(f"Duplicate value: {value}")
                inv_items[value] = key
            self.inv = BiDict(inv_items, self)
        else:
            self.inv = _inv

    def __setitem__(self, key: K, value: V) -> None:
        if key in self:
            super(BiDict, self.inv).__delitem__(self[key])
        if value in self.inv:
            super().__delitem__(self.inv[value])
        super().__setitem__(key, value)
        super(BiDict, self.inv).__setitem__(value, key)

    def __delitem__(self, key: K) -> None:
        value = self[key]
        super().__delitem__(key)
        super(BiDict, self.inv).__delitem__(value)
