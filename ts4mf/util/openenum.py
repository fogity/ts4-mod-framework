"""A non-strict enum implementation."""
from __future__ import annotations
import inspect
import re
from typing import Any, Dict, Iterator, Optional, Type, TypeVar


def _as_enum_name(string: str) -> str:
    """Converts a string into a (possibly) valid enum name."""
    string = re.sub(r"[()]", "", string)
    string = re.sub(r"[ -]", "_", string)
    return string.upper()


class name_as_value:
    """Used in place of an enum value to use the name as the value."""

    def __init__(self) -> None:
        self._name: Optional[str] = None


class _OpenEnumMeta(type):
    """Metaclass for the open enum class.

    Should only be used by the OpenEnum base class.
    """

    def __init__(cls, name: str, bases, namespace: Dict[str, Any]) -> None:
        # Set default sunder members.
        if not hasattr(cls, "_cache_unnamed_"):
            cls._cache_unnamed_ = True
        if not hasattr(cls, "_generate_value_"):
            cls._generate_value_ = lambda value: value
        if not hasattr(cls, "_str_value_"):
            cls._str_value_ = str
        # Set up enum indicies.
        cls._name_to_enum: Dict[str, Any] = {}
        cls._value_to_enum: Dict[Any, Any] = {}
        # Convert class members to emum values.
        for name, value in namespace.items():
            # Ignore non-enum members.
            if not name.isupper():
                continue
            # Let setattr convert the member to an enum.
            setattr(cls, name, value)

    # Make calling the enum class returned cached values if available.
    def __call__(cls, *args, **kwargs) -> Any:
        # Check the arguments.
        try:
            inspect.signature(cls._generate_value_).bind(*args, **kwargs)
        except TypeError:
            raise TypeError(f"Invalid arguments to {cls.__name__}")
        # Convert the arguments to the enum value.
        value = cls._generate_value_(*args, **kwargs)
        # Look up the value in the value index.
        if value in cls._value_to_enum:
            return cls._value_to_enum[value]
        # Create a new enum value.
        enum = super().__call__(value)
        # Register value if caching_ is enabled.
        if cls._cache_unnamed_:
            cls._value_to_enum[value] = enum
        # Return the enum.
        return enum

    # Allow getting enums by name through indexing.
    def __getitem__(cls, string: str) -> Any:
        if not isinstance(string, str):
            raise ValueError("Only strings are allowed in enum lookups")
        # Transform the string into an enum name.
        name = _as_enum_name(string)
        # Check if the enum name exists.
        if name not in cls._name_to_enum:
            raise LookupError(f"Not a valid {cls.__name__}: {string}")
        # Return the named enum.
        return cls._name_to_enum[name]

    # Allow iterating through named enums (excluding aliases).
    def __iter__(cls) -> Iterator[Any]:
        return (
            enum
            for name, enum in cls.__dict__.items()
            if name.isupper() and enum.name == name
        )

    # Automatically convert assignments with enum names to enums.
    def __setattr__(cls, name: str, value: Any) -> None:
        # Ignore non-enum members.
        if not name.isupper():
            return super().__setattr__(name, value)
        # Ensure the name is not already in use.
        if name in cls._name_to_enum:
            raise AttributeError("Can not change enum values")
        # Handle if 'name_as_value' was given.
        if isinstance(value, name_as_value):
            # If a name is already set, use it.
            if value._name:
                value = value._name
            # Otherwise assign the current name.
            else:
                value._name = name
                value = name
        # Handle if an enum was given.
        if isinstance(value, cls):
            enum = value
        # Otherwise use the value as enum argument.
        else:
            enum = cls(value)
        # Ensure the enum is cached.
        if enum.value not in cls._value_to_enum:
            cls._value_to_enum[enum.value] = enum
        # Register the enum in the name index.
        cls._name_to_enum[name] = enum
        # Set the enum name vaiable if not already set.
        if not enum._name:
            enum._name = name
        # Assign to the class.
        super().__setattr__(name, enum)


# Type variable for OpenEnum subclasses.
_E = TypeVar("_E", bound="OpenEnum")


class OpenEnum(metaclass=_OpenEnumMeta):
    """A non-strict enum class."""

    # Use a general signature to reduce false type checking positives.
    def __init__(self, *args, **kwargs) -> None:
        self._value: Any = args[0]
        self._name: Optional[str] = None

    @property
    def value(self) -> Any:
        """The value of the enum."""
        return self._value

    @property
    def name(self) -> Optional[str]:
        """The name of the enum, if it has one."""
        return self._name

    @classmethod
    def lookup(cls: Type[_E], value: Any) -> _E:
        """Looks up an enum by name or value.

        Args:
            value: A name or value for the enum.

        Returns:
            The corresponding enum value.

        Raises:
            LookupError: If no corresponding enum exists.
        """
        if isinstance(value, cls):
            return value
        try:
            return cls[value]  # type: ignore
        except (LookupError, ValueError):
            try:
                return cls(value)  # type: ignore
            except ValueError:
                raise LookupError(f"Not a valid {cls.__name__} name or value: {value}")

    def __eq__(self, other: Any) -> bool:
        # Check if the value matches.
        if self.value == other:
            return True
        if not isinstance(other, str):
            return False
        # Check if the name is a valid match.
        try:
            return self.value == self.__class__[other]  # type: ignore
        except LookupError:
            return False

    def __hash__(self) -> int:
        return hash(self.value)

    def __str__(self) -> str:
        return self.name or self.__class__._str_value_(self.value)  # type: ignore

    def __repr__(self) -> str:
        if self.name:
            return f"{self.__class__.__name__}.{self.name}"
        return f"{self.__class__.__name__}({self.value!r})"


class _OpenHashEnumMeta(_OpenEnumMeta):
    """Metaclass for the open hash enum class.

    Should only be used by the OpenHashEnum base class.
    """

    def __init__(cls, name: str, bases, namespace: Dict[str, Any]):
        # Set default sunder members.
        if not hasattr(cls, "_generate_hash_"):
            cls._generate_hash_ = hash
        # Ensure everything else is set up.
        super().__init__(name, bases, namespace)

    # Add the hashing step when constructing an enum.
    def __call__(cls, *args, **kwargs):
        # Hash the first argument if it is a string.
        if len(args) > 0 and isinstance(args[0], str):
            string = args[0]
            hash = cls._generate_hash_(string)
            args = hash, *args[1:]
        else:
            string = None
        # Get the enum as usual.
        enum = super().__call__(*args, **kwargs)
        # Extra steps for hashed enums.
        if string is not None:
            # Ensure the enum is cached.
            if enum.value not in cls._value_to_enum:
                cls._value_to_enum[enum.value] = enum
            # Set the enum string variable if not already set.
            if not enum._string:
                enum._string = string
        # Return the enum.
        return enum


class OpenHashEnum(OpenEnum, metaclass=_OpenHashEnumMeta):
    """A non-strict enum class with support for value hashing."""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._string: Optional[str] = None

    @property
    def string(self) -> Optional[str]:
        """The string hashed to the enum value, if it is available."""
        return self._string

    def __eq__(self, other: Any) -> bool:
        if super().__eq__(other):
            return True
        if not isinstance(other, str):
            return False
        # Check if the hash matches.
        return self.value == self.__class__._generate_hash_(other)  # type: ignore

    def __hash__(self) -> int:
        return self.value

    def __str__(self) -> str:
        return self.name or self.string or super().__str__()

    def __repr__(self) -> str:
        if not self.name and self.string:
            return f"{self.__class__.__name__}({self.string!r})"
        return super().__repr__()
