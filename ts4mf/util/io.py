"""Convenience functions for file I/O."""
from io import BytesIO
from struct import calcsize, pack, unpack
from typing import Any, IO, Tuple


def to_stream(arg) -> IO[bytes]:
    """Convert the argument into a binary stream.

    If the argument is a byte string or supports byte string conversion,
    then an in-memory stream is created from the byte string.
    If the argument is a string it is used as the filename for 'open'.
    Otherwise the argument is assumed to be a binary stream already.

    Args:
        arg: The argument to convert.

    Returns:
        A binary stream.
    """
    if isinstance(arg, bytes):
        return BytesIO(arg)
    if hasattr(arg, "__bytes__"):
        return BytesIO(bytes(arg))
    if isinstance(arg, str):
        return open(arg, "rb")
    return arg


def fpack(format: str, file: IO[bytes], *values) -> None:
    """Convenience function for packing to a file."""
    file.write(pack(format, *values))


def funpack(format: str, file: IO[bytes]) -> Tuple[Any, ...]:
    """Convenience function for unpacking from a file."""
    return unpack(format, file.read(calcsize(format)))


def funpack1(format: str, file: IO[bytes]) -> Any:
    """Convenience function for unpacking from a file."""
    return funpack(format, file)[0]
