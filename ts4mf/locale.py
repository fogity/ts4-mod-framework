"""Module for handling localisation."""
from functools import partial

from ts4mf.util.asserts import assert_uint_size
from ts4mf.util.openenum import OpenEnum


class LocaleCode(OpenEnum):
    """Codes used for localisation.

    Args:
        value: A locale id byte.
    """

    ENG_US = 0x00
    ENGLISH = ENG_US
    # No official translation
    CHS_CN = 0x01
    CHINESE_SIMPLIFIED = CHS_CN
    CHT_CN = 0x02
    CHINESE = CHT_CN
    CHINESE_TRADITIONAL = CHT_CN
    CZE_CZ = 0x03
    CZECH = CZE_CZ
    DAN_DK = 0x04
    DANISH = DAN_DK
    DUT_NL = 0x05
    DUTCH = DUT_NL
    FIN_FI = 0x06
    FINISH = FIN_FI
    FRE_FR = 0x07
    FRENCH = FRE_FR
    GER_DE = 0x08
    GERMAN = GER_DE
    # No official translation
    GRE_GR = 0x09
    GREEK = GRE_GR
    # No official translation
    HUN_HU = 0x0A
    HUNGARIAN = HUN_HU
    ITA_IT = 0x0B
    ITALIAN = ITA_IT
    JPN_JP = 0x0C
    JAPANESE = JPN_JP
    KOR_KR = 0x0D
    KOREAN = KOR_KR
    NOR_NO = 0x0E
    NORWAY = NOR_NO
    POL_PL = 0x0F
    POLISH = POL_PL
    # No official translation
    POR_PT = 0x10
    PORTUGUESE_PORTUGAL = POR_PT
    POR_BR = 0x11
    PORTUGUESE = POR_BR
    PORTUGUESE_BRAZIL = POR_BR
    RUS_RU = 0x12
    RUSSIAN = RUS_RU
    SPA_ES = 0x13
    SPANISH = SPA_ES
    SPANISH_SPAIN = SPA_ES
    # No official translation
    SPA_MX = 0x14
    SPANISH_MEXICO = SPA_MX
    SWE_SE = 0x15
    SWEDISH = SWE_SE
    # No official translation
    THA_TH = 0x16
    THAI = THA_TH

    # Limit to 8 bit unsigned integers.
    _generate_value_ = partial(assert_uint_size, 8, source="LocaleCode")
    # Use hexadecimal format.
    _str_value_ = "0x{:02X}".format
