"""Module for working with resources, particularly resource keys."""
from contextlib import suppress
from dataclasses import dataclass
from functools import partial
import re
from typing import Optional, Union

from ts4mf.hash import fnv
from ts4mf.locale import LocaleCode
from ts4mf.util.asserts import assert_uint_size
from ts4mf.util.openenum import OpenHashEnum, name_as_value


class ResourceType(OpenHashEnum):
    """Type component of a resource key.

    Args:
        value: A type id or a type name to hash.
    """

    STBL = name_as_value()
    STRINGTABLE = STBL
    STRING_TABLE = STBL

    # Use 32 bit FNV-1 hash.
    _generate_hash_ = partial(fnv, 32)
    # Limit to 32 bit unsigned integers.
    _generate_value_ = partial(assert_uint_size, 32, source="ResourceType")
    # Use hexadecimal format.
    _str_value_ = "0x{:08X}".format


class ResourceGroup(OpenHashEnum):
    """Group component of a resource key.

    Args:
        value: A group id or a group name to hash.
    """

    MOD = 0x80000000

    # Use 24 bit FNV-1 hash.
    _generate_hash_ = partial(fnv, 24)
    # Limit to 32 bit unsigned integers.
    _generate_value_ = partial(assert_uint_size, 32, source="ResourceGroup")
    # Use hexadecimal format.
    _str_value_ = "0x{:08X}".format


class ResourceInstance(OpenHashEnum):
    """Instance component of a resource key.

    Args:
        value: An instance id or instance name to hash.
        tag: Either the string 'mod' to set the high bit
            or a locale code to set as the high byte.
    """

    # Override the init method in order to store the tag.
    def __init__(self, *args, **kwargs):
        # Split the value and tag.
        value, tag = args[0]
        # Call the super init with just the value.
        super().__init__(value)
        # Store the tag value.
        self._tag = tag

    @property
    def tag(self) -> Optional[Union[str, LocaleCode]]:
        """The tag of the instance value."""
        return self._tag

    # Do not cache all instance values.
    _cache_unnamed_ = False
    # Use 64 bit FNV-1 hash.
    _generate_hash_ = partial(fnv, 64)
    # Use hexadecimal format.
    _str_value_ = "0x{:016X}".format

    # Return the tag as well for storing in the init method.
    @staticmethod
    def _generate_value_(value, tag=None):
        # Limit to 64 bit unsigned integers.
        assert_uint_size(64, value, source="ResourceInstance")
        # If no tag is specified, just return the value.
        if tag is None:
            return value, None
        # Set the high bit if marked as a mod.
        if tag == "mod":
            return 0x8000000000000000 | value, "mod"
        # Interpret the tag as a locale code and set the high byte.
        locale = LocaleCode.lookup(tag)
        return (locale.value << 56) + (0x00FFFFFFFFFFFFFF & value), locale

    def __repr__(self) -> str:
        if not self.name and self.tag:
            return (
                f"{self.__class__.__name__}"
                f"({self.string or self.value!r}, {self.tag!r})"
            )
        return super().__repr__()


@dataclass(frozen=True)
class ResourceKey:
    """A resource key used to uniquely identify a resource.

    Attributes:
        type: The resource type.
        group: The resource group.
        instance: The resource instance.
    """

    type: ResourceType
    group: ResourceGroup
    instance: ResourceInstance

    def __init__(self, type, group, instance, tag=None):
        """
        Args:
            type: A valid ResourceType argument.
            group: A valid ResourceGroup argument.
            instance: A valid ResourceInstance value argument.
            tag: A valid ResourceInstance tag argument.
        """
        object.__setattr__(self, "type", ResourceType.lookup(type))
        object.__setattr__(self, "group", ResourceGroup.lookup(group))
        if tag is None:
            instance = ResourceInstance.lookup(instance)
        else:
            instance = ResourceInstance(instance, tag)
        object.__setattr__(self, "instance", instance)

    def __str__(self) -> str:
        if self.instance.tag:
            return (
                "ResourceKey"
                f"({self.type}, {self.group}, {self.instance}, {self.instance.tag})"
            )
        return f"ResourceKey({self.type}, {self.group}, {self.instance})"


class FilenameMismatch(ValueError):
    """Raised when a filename pattern did not match."""


def from_filename(filename: str) -> ResourceKey:
    """Generate a resource key from a filename.

    Supported formats include ts4mf-style, Maxis-style, s4pe-style
    and Sims4Studio-style.
    """
    # Start with s4pe format to not incorrectly match ts4mf format.
    with suppress(FilenameMismatch):
        return from_s4pe_filename(filename)
    with suppress(FilenameMismatch):
        return from_ts4mf_filename(filename)
    with suppress(FilenameMismatch):
        return from_maxis_filename(filename)
    with suppress(FilenameMismatch):
        return from_s4s_filename(filename)
    raise ValueError("Not a valid filename")


# Resource types with extended type components.
_extended_resource_types = [ResourceType.STBL]


def _type_from_ts4mf_filename_parts(type, exts) -> ResourceType:
    """Determines the resource type from selected ts4mf filename parts.

    Args:
        type: The type part of the filename.
        exts: The extensions of the filename.
    """
    # If no type is given, use the extensions instead.
    if not type:
        # If no extension is given, raise an error.
        if not exts:
            raise ValueError("Filename does not contain a resource type")
        # Special cases for extended types.
        for t in _extended_resource_types:
            if any(map(lambda e: e == t, exts)):
                return t  # type: ignore
        # The first extension is treated as the resource type.
        type = exts[0]
    # Convert the string if it is a number.
    with suppress(ValueError):
        type = int(type, 0)
    # Look up and return the type.
    return ResourceType.lookup(type)


def _group_from_ts4mf_filename_parts(group, flags) -> ResourceGroup:
    """Determines the resource type from selected ts4mf filename parts.

    Args:
        group: The group part of the filename.
        flags: The flags from the filename.
    """
    if group:
        # Convert the string if it is a number.
        with suppress(ValueError):
            group = int(group, 0)
        # Look up and return the group.
        return ResourceGroup.lookup(group)
    # Set the mod group if mod flag is given.
    if "mod" in flags:
        return ResourceGroup.MOD  # type: ignore
    # Otherwise, set group to 0.
    return ResourceGroup(0)


# Custom resource key string format (IGT) for extra features.
_ts4mf_regex = re.compile(
    r"([\w_]+)(?:-([\w_]+))?(?:-([\w_]+))?((?:\+[\w_]+)*)((?:\.[\w_]+)*)"
)
_ts4mf_flag_regex = re.compile(r"(?<=\+)[\w_]+")
_ts4mf_ext_regex = re.compile(r"(?<=\.)[\w_]+")


def from_ts4mf_filename(filename: str) -> ResourceKey:
    """Generate a resource key from a ts4mf-style filename."""
    match = _ts4mf_regex.fullmatch(filename)
    if not match:
        raise FilenameMismatch("Not a ts4mf-style filename")
    # Extract the values from the filename.
    instance, group, type, flags, exts = match.groups()
    flags = list(map(str.lower, _ts4mf_flag_regex.findall(flags)))
    exts = list(map(str.lower, _ts4mf_ext_regex.findall(exts)))
    # Handle the resource type.
    type = _type_from_ts4mf_filename_parts(type, exts)
    # Handle the resource group.
    group = _group_from_ts4mf_filename_parts(group, flags)
    # Convert the instance if it is a number.
    with suppress(ValueError):
        instance = int(instance, 0)
    # Handle the instance tag.
    tag = None
    if type == ResourceType.STBL:
        # Use the string table type extension as the locale.
        if len(exts) >= 2 and exts[1] == ResourceType.STBL:
            locale = exts[0]
            with suppress(ValueError):
                locale = int(locale, 0)
            tag = LocaleCode.lookup(locale)
        # Raise an error if no locale is specified and the instance is not an id.
        elif not isinstance(instance, int):
            raise ValueError("No locale was specified for the string table")
    elif "mod" in flags:
        tag = "mod"
    # Construct the resource key.
    return ResourceKey(type, group, instance, tag)


# Maxis-style resource key string format (GIT).
_maxis_regex = re.compile(
    r"(0x[0-9a-fA-F]+|\d+)!(0x[0-9a-fA-F]+|\d+)\.(0x[0-9a-fA-F]+|\d+)"
)


def from_maxis_filename(filename: str) -> ResourceKey:
    """Generate a resource key from a Maxis-style filename."""
    match = _maxis_regex.fullmatch(filename)
    if not match:
        raise FilenameMismatch("Not a Maxis-style filename")
    group, instance, type = map(partial(int, base=0), match.groups())
    return ResourceKey(type, group, instance)


# s4pe-style resource key string format (TGI).
_s4pe_regex = re.compile(r"S4_([0-9a-fA-F]+)_([0-9a-fA-F]+)_([0-9a-fA-F]+).*")


def from_s4pe_filename(filename: str) -> ResourceKey:
    """Generate a resource key from a s4pe-style filename."""
    match = _s4pe_regex.fullmatch(filename)
    if not match:
        raise FilenameMismatch("Not a s4pe-style filename")
    type, group, instance = map(partial(int, base=16), match.groups())
    return ResourceKey(type, group, instance)


# Sims4Studio-style resource key string format (TGI).
_s4s_regex = re.compile(r"([0-9a-fA-F]+)!([0-9a-fA-F]+)!([0-9a-fA-F]+).*")


def from_s4s_filename(filename: str) -> ResourceKey:
    """Generate a resource key from a Sims4Studio-style filename."""
    match = _s4s_regex.fullmatch(filename)
    if not match:
        raise FilenameMismatch("Not a Sims4Studio-style filename")
    type, group, instance = map(partial(int, base=16), match.groups())
    return ResourceKey(type, group, instance)


def to_filename(key: ResourceKey, format: str) -> str:
    """Generate a filename based on a resource key.

    Supported formats include ts4mf-style, Maxis-style, s4pe-style
    and Sims4Studio-style.
    """
    fmt = format.lower()
    if fmt == "ts4mf":
        return to_ts4mf_filename(key)
    if fmt == "maxis":
        return to_maxis_filename(key)
    if fmt == "s4pe":
        return to_s4pe_filename(key)
    if fmt == "s4s":
        return to_s4s_filename(key)
    raise ValueError(f"Unsupported filename format: {format}")


def to_ts4mf_filename(key: ResourceKey) -> str:
    """Generates a ts4mf-style filename based on a resource key."""
    # Handle extensions (type is handled by the extension).
    if key.type == ResourceType.STBL:
        locale = LocaleCode(key.instance.value >> 56)
        exts = f".{locale}.stbl"
    else:
        exts = f".{key.type}"
    # Check mod flag.
    mod = key.group == ResourceGroup.MOD and (
        key.type == ResourceType.STBL or 0x8000000000000000 & key.instance.value
    )
    # Handle flags:
    flags = "+mod" if mod else ""
    # Handle group.
    if not key.group.value or (mod and key.group == ResourceGroup.MOD):
        group = ""
    else:
        group = f"-{key.group}"
    # Put it together.
    return f"{key.instance}{group}{flags}{exts}"


def to_maxis_filename(key: ResourceKey) -> str:
    """Generates a Maxis-style filename based on a resource key."""
    return (
        f"0x{key.group.value:08X}!0x{key.instance.value:016X}"
        f".0x{key.type.value:08X}"
    )


def to_s4pe_filename(key: ResourceKey) -> str:
    """Generates a s4pe-style filename based on a resource key."""
    return f"S4_{key.type.value:08X}_{key.group.value:08X}_{key.instance.value:016X}"


def to_s4s_filename(key: ResourceKey) -> str:
    """Generates a Sims4Studio-style filename based on a resource key."""
    return f"{key.type.value:08X}!{key.group.value:08X}!{key.instance.value:016X}"


def to_resource_key(*args) -> ResourceKey:
    """Tries to create a resource key from the arguments.

    Accepts any of the following:
        An existing resource key.
        Arguments to 'ResourceKey' (position only).
        A tuple containing arguments to 'ResourceKey'.
        A resource filename in a supported format.
    """
    if len(args) == 1:
        arg = args[0]
        if isinstance(arg, ResourceKey):
            return arg
        if isinstance(arg, str):
            return from_filename(arg)
        if not isinstance(arg, tuple):
            raise ValueError("Invalid arguments")
        args = arg
    if 3 <= len(args) <= 4:
        return ResourceKey(*args)
    raise ValueError("Invalid arguments")
