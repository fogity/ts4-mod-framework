"""Module for looking up game files."""
from dataclasses import dataclass
from itertools import chain
import os
import re
from typing import Callable, Iterator, Optional, Union

from ts4mf.env import Pack, ts4_game_dir
from ts4mf.locale import LocaleCode
from ts4mf.resource import ResourceType
from ts4mf.util.openenum import OpenEnum


@dataclass(frozen=True)
class _PackageTypeInfo:
    """Information about a package type.

    Attributes:
        basegame_path: Location of the files for the basegame.
        pack_path: Location of the files for packs.
            Set to None if the files do not appear in packs.
            Use '{pack}' to insert the pack name in the path.
        filename_regex: Regex used to identify a file.
        locale_index: Index of the match group for the locale.
            Set to None if locale is not present.
    """

    basegame_path: str
    pack_path: Optional[str]
    filename_regex: re.Pattern
    locale_index: Optional[int]


class _PackageType(OpenEnum):
    """The different package types."""

    # Localisation packages.
    STRINGS = _PackageTypeInfo(
        basegame_path=os.path.join("Data", "Client"),
        pack_path="{pack}",
        filename_regex=re.compile(r"Strings_(\w{3}_\w{2}).package"),
        locale_index=1,
    )


class UnsupportedLookup(Exception):
    """Raised when a query is not yet supported."""


def _find_packages_by_package_type(
    ptype: _PackageType, locale_pred: Callable[[LocaleCode], bool] = None,
) -> Iterator[str]:
    """Finds all packages belonging to the specified package type.

    Args:
        ptype: The package type.
        locale_pred: Predicate used to filter on locales.

    Yields:
        An iterator over the matching package paths.
    """
    pinfo = ptype.value
    game_dir = ts4_game_dir()
    # Generate the paths for directories containing the packages.
    paths = chain(
        [pinfo.basegame_path], (pinfo.pack_path.format(pack=pack.name) for pack in Pack)
    )
    # Check each directory.
    for path in paths:
        full_dir = os.path.join(game_dir, path)
        # Check each file.
        for filename in os.listdir(full_dir):
            # Check if the filename matches.
            match = pinfo.filename_regex.match(filename)
            if not match:
                continue
            # Possibly check locale.
            if locale_pred and pinfo.locale_index:
                locale = LocaleCode.lookup(match.group(pinfo.locale_index))
                if not locale_pred(locale):
                    continue
            # Yield the file path.
            yield os.path.join(full_dir, filename)


# Used to look up packages based on resource type.
_resource_type_to_package_type = {ResourceType.STBL: _PackageType.STRINGS}


def find_packages_by_type(
    type: Union[int, str, ResourceType],
    locale_pred: Callable[[LocaleCode], bool] = None,
) -> Iterator[str]:
    """Finds all packages containing the specified resource type.

    Args:
        type: The resource type.
        locale_pred: Predicate used to filter on locales.

    Yields:
        An iterator over the matching package paths.
    """
    # Get a concrete resource type.
    type = ResourceType.lookup(type)
    # Look up the package type.
    if type not in _resource_type_to_package_type:
        raise UnsupportedLookup(f'Finding package by "{type}" not yet supported')
    ptype = _resource_type_to_package_type[type]  # type: ignore
    # Look up matching packages of that package type.
    yield from _find_packages_by_package_type(ptype, locale_pred)
