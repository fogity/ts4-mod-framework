# TS4 Mod Framework

TS4 Mod Framework (ts4mf) consists of a Python library and command line tools for creating modifications (mods) for The Sims 4.

The command line tools can be used to work with the proprietary file formats used by the game in a convenient manner.

The library itself is designed to be used either outside of the game or from scripts running inside the game.
The focus for use outside of the game include modding tools and scripts for generating mods.
The focus for use inside the game is compatibility across patches and simplicity.

The project is currently in a very early alpha stage, with only a handful of features.
The focus at this time has been solely on the use outside of the game.

## Installation

There are two main ways to install the framework, either as a Python package with command line tools or as a script mod.

The framework requires Python 3.7.0, this is the version bundled with the game.

To install the package and tools:

1. Clone or download this repositry.
2. Make sure the right version of Python is installed, globally or using `venv`.
3. Run `pip install .`in root of the repository.

Now it should be possible to use the library in scripts and to access the tools on the command line.

There is currently no real benefit to using the framework from within the game.
In the future there will be an official release of the framework for use within the game.
To prevent conflicts and outdated versions no script mod should bundle the framework and instead direct the mod user to the official release page.

## Usage

The command line tools each have their own help text that is printed when passing the `-h` flag.
This should give all necessary information for using the tools.
However, in the future there should be more extensive documentation available.

The current command line tools include:

- `ts4mf_hash` for calculating hashes.
- `ts4mf_package` for working with package files.
- `ts4mf_string_table` for working with string tables.

The Python library consists of a number of modules that can be imported or used in any Python program.
They are designed to be flexible and fairly independant from each other.
The library currently has quite a few limitations, and the documentation exists but is a bit bare-bones.
The focus on the current release is the command line tools, but feel free to try out the library as well.

## Contributing

At this point, I am not looking for contributions and would rather ponder and implement features on my own.
This might very well change in the future.

However, I would happily accept feedback and bug reports.
The best way to submit those, or to discuss the framework with me is on [discord](https://discord.gg/YHNG5vc).
