from distutils.core import setup

setup(
    name="ts4mf",
    version="0.0.0",
    description="Mod framework for The Sims 4",
    packages=["ts4mf"],
    entry_points={
        "console_scripts": [
            "ts4mf_hash=ts4mf.tools.hash:main",
            "ts4mf_package=ts4mf.tools.package:main",
            "ts4mf_string_table=ts4mf.tools.string_table:main",
        ]
    },
)
